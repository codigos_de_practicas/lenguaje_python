#########################################################
#Autor: Axel Diaz<Axelio>				#
#Descripcion: Graficos					#
#Fecha: 6/3/2011					#
#Correo: diaz.axelio@gmail.com				#
#########################################################

##################### Programa ##########################
# Iteraciones 						#
#							#
# Un programa que imprima los primeros 10 numeros	#
#########################################################


#################### Comandos ###########################
#							#
# While:						#
# while condicion:					#
#	ordenes a repetir				#
#							#
# For:							#
# for Variable in serie_serie_de_valores []:		#
#	Acciones a repetir				#
#							#
#########################################################


print "--------------Programa para las iteraciones----------------\n\n\n"

rep='s'
num=0

while rep=='s':

########################## Menu #######################
	print "1.- Repeticiones While (Mientras que...)"
	print "2.- Repeticiones For (Para .... Hacer...)"
	print "3.- Salir"

	opcion = int(raw_input("Selecione una opcion: "))


################### Acciones del Menu #################
	if opcion==1:
		print "La sentencia es:\n\twhile Condicion:\n\t\tAcciones\n\nPor ejemplo los numeros del 1 al 10:\n\n---------------Ejemplo-----------\n"
		while num<10:
			num +=1
			print num
	
	elif opcion==2:
		print "La sentencia es:\n\tfor Variable in [Valores_de_serie]:\n\nPor ejemplo un Quiero jugar al...:\n\n----------Ejemplo----------\n"
		for deporte in ["Futbol","Beisbol","Voleibol"]:
			print "Quiero jugar",deporte

################### Ocion Incorrecta #################
	elif opcion==3:
		print "Gracias por revisar esta aplicacion.\n\tAxelio..."
		rep='n'
	elif opcion>1:
		print "Opcion incorrecta!\n"

	if opcion<3:
		rep=(raw_input("\nDesea repetir el programa de nuevo? (S/N) "))

