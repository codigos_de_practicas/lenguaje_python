#########################################################
#Autor: Axel Diaz<Axelio>                               #
#Fecha: 6/3/2011                                        #
#Correo: diaz.axelio@gmail.com                          #
#########################################################

###################### ENUNCIADO ########################
#							#
# Realice un programa en Python que calcule		#
# el factorial de un numero 				#
#							#
#########################################################

######################## PROGRAMA #######################

resp='s'

print"Aplicacion para calculo de factoriales..."
while resp=='s':
	fact=int(1)	
	num=int(raw_input("Numero: "))
	
	for rep in range(1,num+1):
		fact = fact * rep
	
	print "El factorial de %d es %d" %(num,fact)

	resp=raw_input("Desea repetir esta aplicacion? (S/N) ")

print "Gracias por iniciar esta aplicacion...\n\tAxelio"
