#!/usr/bin/env python
#example base.py

import pygtk
pygtk.require('2.0')
import gtk

class Base:
    def hello(self, widget, data=None):
       print "Hola mundo mundisimo!"
    def delete_event(self, widget, event, data=None):
       print "Borrado el evento ocurrido"
       
       return gtk.FALSE
     
    def destroy(self, widget, data=None):
       gtk.main_quit()

    def _init_(self):
       self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
       self.window.connect("delete_event", self.delete_event)
       self.window.connect("destroy", self.destroy)
       self.button = gtk.Button("Hola mundo mundisimo!")
       self.button.connect("clicked", self.hello, None)
       self.window.add(self.button)
       self.button.show()
       self.window.show()

    def main(self):
       gtk.main()

if __name__== "__main__":
    base = Base()
    base.main()
