#########################################################
#Autor: Axel Diaz<Axelio>                               #
#Fecha: 6/3/2011                                        #
#Correo: diaz.axelio@gmail.com                          #
#########################################################

###################### ENUNCIADO ########################
#							#
# 1. Realice un programa en Python que:			#
#  a.  Se le ingresen dos numero (a y b).		#
#  b.  Si los dos numeros son iguales, los sume.	#
#  c.  Si son diferentes, reste el mayor menos el menor.#
#  d.  Muestre el resultado de la operacion.		#
#							#
#########################################################

######################## PROGRAMA #######################

print "Esta aplicacion al ingresar dos numeros hara:\n\tA) Si los numeros son iguales se sumaran\n\tB) Si los numeros son diferentes restara el mayor menos el menor\n\tD) Mostrara el resultado de la operacion\n\n\n"
print "----------------- Programa --------------\n\n"

resp = 's'
while resp=='s':

	variable_a = float(raw_input("Ingrese el primer valor: "))
	variable_b = float(raw_input("Ingrese el segundo valor: "))

	if variable_a == variable_b:
		resultado = variable_a + variable_b
	else:
		if variable_a > variable_b:
			resultado = variable_a - variable_b
		else:
			resultado = variable_b - variable_a

	print "El resultado es: %f" %(resultado)

	resp = raw_input("\n\nDesea Repetir la aplicacion? (S/N) ")
print "Gracias por iniciar esta aplicacion...\n\tAxelio"
