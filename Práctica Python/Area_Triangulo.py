#########################################################
#Autor: Axel Diaz<Axelio>				#
#Descripcion: Ejercicio 1 de la guia 1			#
#Fecha: 6/3/2011					#
#Correo: diaz.axelio@gmail.com				#
#########################################################

######################## ENUNCIADO ######################
# Realice un programa en Python que:			#
# Calcule el area de un triangulo			#
#########################################################

######################## PROGRAMA #######################
print "Programa de Calculo del Area de un triangulo\n\n\a"

base = float(raw_input("Medida de la Base: "))
altura = float(raw_input("Medida de la Altura: "))

area = base * altura / 2.0

#el % es para una serie de comandos cortos, la f es para señalar flotante
#la d es entero y s string.

#el 0 son los espacios antes del valor y el .3 son 3 decimales
print "El area del triangulo es %0.3f:" %(area)
