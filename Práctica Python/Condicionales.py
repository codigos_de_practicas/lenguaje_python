#########################################################
#Autor: Axel Diaz<Axelio>				#
#Descripcion: Graficos					#
#Fecha: 6/3/2011					#
#Correo: diaz.axelio@gmail.com				#
#########################################################

######################## ENUNCIADO ######################
# Como hacer las condicionales en python		#
# Para determinar si un usuario es mayor de edad o no	#
#########################################################

######################## PROGRAMA #######################

#if condicion:
	#Procedimiento_del_condicional

print "-------------Programa para la sentencia IF-------------\n\n\n"


edad=int(raw_input("Por favor, inserte su edad: "))

#si...
if edad>=18:
	print "Usted es mayor de edad"

elif edad>12:
	print "Usted es a penas un adolescente"

#sino...
else:
	print "Usted es un nino"
