#########################################################
#Autor: Axel Diaz<Axelio>				#
#Descripcion: Revisar si el numero es primo o no	#
#Fecha: 6/3/2011					#
#Correo: diaz.axelio@gmail.com				#
#########################################################

######################## ENUNCIADO ######################
# Realice un programa en Python que:			#
# Diga si el numero ingresado es primo o no		#
#########################################################

######################## PROGRAMA #######################
print "Este programa consiste en decir si el numero es primo o no...\n\n"

resp='s'
while resp=='s':
	numero = int(raw_input("Numero: "))
	
	es_primo=True
	
	for divisor in range(2,numero):
		if numero%divisor == 0:
			es_primo=False
			break
	if es_primo:
		print "El numero es primo!"
	else: 
		print "El numero no es primo!"
	resp=raw_input("\n\nDesea repetir la aplicacion? (S/N) ")

print "Gracias por abrir esta aplicacion...\n\tAxelio"
