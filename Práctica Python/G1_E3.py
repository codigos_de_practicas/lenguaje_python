#########################################################
#Autor: Axel Diaz<Axelio>                               #
#Descripcion: Ejecicio 1 de la Guia 1                   #
#Fecha: 6/3/2011                                        #
#Correo: diaz.axelio@gmail.com                          #
#########################################################

###################### ENUNCIADO ########################
#							#
# Realice un programa en Python que calcule el promedio	#
# de 10 notas, e identifique si el alumno se encuentra 	#
# reprobado o aprobado, (nota entre 1 y 10 puntos, 	#
# nota minima aprobatoria 6 puntos).			#
#							#
#########################################################


######################## PROGRAMA #######################
resp='s'

print "Programa para calcular el promedio de 10 notas\n\n\n"
while resp=='s':
    acum=0	
    prom=1

    for i in range(1,11):
        cal=float(raw_input("Nota "))
        while (cal >10 or cal < 1):
            cal=float(raw_input("Nota invalida, Introduzca la nota nuevamente "))

        acum += cal
	
    prom=acum/10
	
    if prom>5:
        print "Felicidades! aprobaste con %f puntos!" %(prom)
    else: 
        print "Mala ahi! reprobaste con %f puntos..." %(prom)


    resp=raw_input("\n\n\nDesea repetir esta aplicacion? (S/N) ")
print "Gracias por iniciar esta aplicacion...\n\tAxelio"
